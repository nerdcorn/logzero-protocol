package protocol

var (
	errorMap = map[uint]string{
		200001: "node cannot establish Connection",
		200010: "stream does not exist",
		200011: "stream access restricted",
		200020: "received corrupted data",
		200021: "unable to parse incoming data",
	}
)

const (
	ErrorCannotEstablishConnection = 200001
	ErrorStreamDoesNotExist        = 200010
	ErrorStreamAccessRestricted    = 200011
	ErrorReceivedCorruptedData     = 200020
	ErrorUnableToParseIncomingData = 200021
)

type (
	ErrorType struct {
		Code    uint
		Message string
	}
)

func NewError(code uint) ErrorType {
	return ErrorType{
		Code:    code,
		Message: errorMap[code],
	}
}
