package protocol

import (
	"encoding/gob"
	"time"
)

const (
	TypeNoMessageResponse  uint = 0000
	TypeHandshakeResponse  uint = 1000
	TypeStreamResponse     uint = 2000
	TypeConnectionResponse uint = 2010
)

type (
	R interface {
		NewError(code uint)
	}

	StreamResponse struct {
		R
		ConnectionId string
		StreamId     string
		Error        ErrorType
	}

	ConnectionResponse struct {
		R
		ConnectionId string
		Error        ErrorType
	}

	// RecordBatchResponse - Once a batch of records is committed, a receipt is awarded
	RecordBatchResponse struct {
		R
		ReceiptId string
		Timestamp time.Time
		Records   []string // Each Commit Id is recorded here
		Error     ErrorType
	}

	HandshakeResponse struct {
		R
		Header  Header
		IsError bool
		Error   ErrorType
	}
)

func init() {
	gob.Register(HandshakeResponse{})
	gob.Register(RecordBatchResponse{})
	gob.Register(StreamResponse{})
}

func NewStreamResponse(connectionId, streamId string) R {
	return StreamResponse{
		ConnectionId: connectionId,
		StreamId:     streamId,
		Error:        ErrorType{},
	}
}

func NewConnectionResponse(connectionId string) R {
	return ConnectionResponse{
		ConnectionId: connectionId,
		Error:        ErrorType{},
	}
}

func NewHandshakeResponse(header Header) R {
	return HandshakeResponse{
		Header:  header,
		IsError: false,
		Error:   ErrorType{},
	}
}

func (sr StreamResponse) NewError(code uint) {
	sr.Error = NewError(code)

}

func (cr ConnectionResponse) NewError(code uint) {
	cr.Error = NewError(code)
}

func (rbr RecordBatchResponse) NewError(code uint) {
	rbr.Error = NewError(code)
}

func (hr HandshakeResponse) NewError(code uint) {
	hr.Error = NewError(code)
}
