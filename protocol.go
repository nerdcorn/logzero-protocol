package protocol

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/gob"
	"fmt"
	"github.com/google/uuid"
	"github.com/op/go-logging"
	"net"
	"time"
)

var log = logging.MustGetLogger("__logzero_protocol__")

type (
	DataType        string
	CompressionType string
	ConnectionType  uint
)

const (
	ProducerType      ConnectionType  = 1
	ConsumerType      ConnectionType  = 2
	BinaryOnly        DataType        = "binary"
	JSONOnly          DataType        = "json"
	NoCompression     CompressionType = "none"
	ZLibCompression   CompressionType = "zlib"
	SnappyCompression CompressionType = "snappy"
)

type (
	Protocol interface {
		Publish(Packet) error
		Receive() (Packet, error)
	}

	P struct {
		encoder       *gob.Encoder
		decoder       *gob.Decoder
		Connection    net.Conn
		Hello         Hello
		Handshake     Handshake
		StreamRequest ConnectStream
	}

	Header struct {
		ConnectionId string // Consumer Connection Id (UUID)
		NodeId       string // Instance id of the node (UUID)
		StreamId     string // A reusable stream id for the consumer to share among additional consumers
		SID          string
		CustomerId   string
	}

	Message struct {
		MessageType uint
		Response    interface{}
	}

	Acknowledgement struct {
		ReceiptId  uuid.UUID
		AckId      uuid.UUID
		ReceivedAt time.Time
	}

	Packet struct {
		Id      uuid.UUID
		Header  Header
		Record  Record
		Message Message
		Ack     Acknowledgement
	}

	Server struct {
		Address    string
		Port       string
		SkipVerify bool
		RootCAs    *x509.CertPool
	}

	Connection struct {
		Header Header
		Conn   net.Conn
	}

	Record struct {
		DataType    DataType
		Compression CompressionType
		Body        []byte
		Receipt     Receipt
	}

	Receipt struct {
		Current string
		Next    string
		Prev    string
	}
)

func New(conn net.Conn) (P, error) {
	return P{
		Connection: conn,
		encoder:    gob.NewEncoder(conn),
		decoder:    gob.NewDecoder(conn),
	}, nil
}

func NewTLSConnection(s Server) (P, error) {
	conn, err := tls.Dial("tcp", fmt.Sprintf("%s:%s", s.Address, s.Port), &tls.Config{
		RootCAs:            s.RootCAs,
		InsecureSkipVerify: s.SkipVerify,
	})
	if err != nil {
		return P{}, err
	}
	return New(conn)
}

func (p *P) Close() {
	if err := p.Connection.Close(); err != nil {
		log.Errorf("unable to close Connection: %#v", err)
	}
}

func (p *P) SendHello(hello Hello) error {
	if err := p.encoder.Encode(&hello); err != nil {
		return err
	}

	return nil
}

func (p *P) ReceiveHello() error {
	if err := p.decoder.Decode(&p.Hello); err != nil {
		return err
	}

	return nil
}

func (p *P) SendHandshake(handshake Handshake) error {
	if err := p.encoder.Encode(&handshake); err != nil {
		return err
	}
	return nil
}

func (p *P) ReceiveHandshake() error {
	if err := p.decoder.Decode(&p.Handshake); err != nil {
		return err
	}
	return nil
}

func (p *P) CreateStream(name, connectionId string, connectionType ConnectionType) error {
	cs := ConnectStream{
		Name:         name,
		ConnectionId: connectionId,
		Connection:   connectionType,
		StreamId:     "",
	}
	if err := p.encoder.Encode(&cs); err != nil {
		return err
	}

	return nil
}

func (p *P) ConnectStream(streamId, connectionId, startingOffset string, connectionType ConnectionType) error {
	cs := ConnectStream{
		StreamId:       streamId,
		ConnectionId:   connectionId,
		Connection:     connectionType,
		StartingOffset: startingOffset,
	}
	if err := p.encoder.Encode(&cs); err != nil {
		return err
	}

	return nil
}

func (p *P) ReceiveStreamInfo() error {
	cs := ConnectStream{}
	if err := p.decoder.Decode(&cs); err != nil {
		return err
	}
	p.StreamRequest = cs
	return nil
}

func (p *P) Receive() (Packet, error) {
	packet := Packet{}
	if err := p.decoder.Decode(&packet); err != nil {
		log.Errorf("unable to receive: %#v", err)
		return packet, err
	}

	return packet, nil
}

func (p *P) Publish(packet Packet) error {
	if err := p.encoder.Encode(&packet); err != nil {
		return err
	}
	return nil
}

func (p *P) Ack(packet Packet) error {
	if err := p.encoder.Encode(&packet); err != nil {
		return err
	}
	return nil
}
