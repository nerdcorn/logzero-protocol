module gitlab.com/nerdcorn/logzero-protocol

go 1.24

require (
	github.com/google/uuid v1.6.0
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
)
