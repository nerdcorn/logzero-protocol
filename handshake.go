package protocol

type (
	Handshake struct {
		ProtocolVersion uint
		ConnectionType  ConnectionType
		CustomerId      string
		Token           string
	}

	// Hello - establishes initial Connection with basic identifiers
	Hello struct {
		ProtocolVersion uint8
		ConnectionId    string
		NodeId          string
	}

	ConnectStream struct {
		ProtocolVersion uint8
		Name            string
		StreamId        string
		ConnectionId    string
		Connection      ConnectionType
		StartingOffset  string
	}
)
